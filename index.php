<!DOCTYPE HTML>

<html>

<head>
<link rel="stylesheet" type="text/css" media="screen" href="styl.css" />
</head>

<body>

    <p>String</p>
    <?php
        $color = "yellow";
        $string = "Milena Miletic";
        echo "<span <span style='color:$color'> " .$string.  "</span>" ;
    ?>

    <p>Int</p>
    <?php
        $color = "red";
        $int = 100;
        echo "<span style='color:$color'> " .$int.  "</span>" ;
    ?>

    <p>Float</p>
    <?php
        $color = "green";
        $float = "150.5";
        echo "<span style='color:$color'> " .$float.  "</span>" ;
    ?>

    <p>BooleanTrueFalse</p>
    <?php
        $color="blue";
        $bool=TRUE;
        echo "<span style='color:$color'> " .$bool. "</span>";
    ?>


    <p>Nizovi</p>
    <?php
        $color = "white";
        $array = array(78, 39, 9, 2, 44, 98, 1);
        foreach($array as $value){
        	echo "<span style='color:$color'> " .$value. "</span>";
        }
    ?>


    <p>NizoviSuma</p>
   	<?php
   	      $color = "purple";
   	      $niz = array(12,2,16,9,1,8);
   	      include 'functions.php';
          echo sumaniza($color , $niz);
          echo "<br>";
          echo "Najmanji broj u nizu je ";
          echo minimum($color, $niz);
          echo "<br>";
          echo "Najveći broj u nizu je ";
          echo maksimum($color, $niz);
   	   ?>
</body>

</html>

